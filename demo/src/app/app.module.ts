import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {XFlowEditor} from "../../../packages/src/core/editor/editor/editor.component";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    XFlowEditor
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
