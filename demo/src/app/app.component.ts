import { Component } from '@angular/core';
import {IBlockModel} from "packages/types";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'demo';

  _data: IBlockModel[] = [
    {
      id: '0002',
      type: 'heading-one',
      props: { },
      nodeType: 'editable',
      children: [
        {insert: 'this is a test header one'}
      ]
    },
    {
      id: 'table-0001',
      type: 'table',
      props: {
        colCount: 2,
        colWidths: [100, 200]
      },
      nodeType: 'block',
      children: [
        {
          id: 'table-row-0001',
          type: 'table-row',
          props: {},
          nodeType: 'block',
          children: [
            {
              id: 'table-cell-0001',
              type: 'table-cell',
              props: { },
              children: [
                {insert: '这是表格单元格内容' + Math.random().toString(36).substring(7)}
              ],
              nodeType: 'editable'
            },
            {
              id: 'table-cell-0002',
              type: 'table-cell',
              props: { },
              children: [
                {insert: '这是表格单元格内容' + Math.random().toString(36).substring(7)}
              ],
              nodeType: 'editable'
            }
          ]
        }
      ]
    },
    {
      id: '0003',
      type: 'heading-two',
      props: {  },
      nodeType: 'editable',
      children: [
        {insert: 'this is a test header one'}
      ],
    },
  ]
}
