import {IInlineAttrs} from "packages/types/inline-attrs";
import {InlineMap} from "packages/types/inline-map";

export interface InsertTextDelta {
  insert: string
  attributes?: ITextStyles
}

/**
 * insert: { [keyof InlineMap]: 'id' }
 */
export interface InsertEmbedDelta {
  insert: { // @ts-ignore
    [p: keyof InlineMap]: string }
  attributes?: IInlineAttrs
}

export interface DeleteDelta {
  delete: number
}

export interface RetainDelta {
  retain: number
  attributes?: ITextStyles
}

export type TextDelta = InsertTextDelta | InsertEmbedDelta
export type Delta = TextDelta | DeleteDelta | RetainDelta

export interface ITextStyles {
  bold?: boolean;
  italic?: boolean;
  underline?: boolean;
  strike?: boolean;
  code?: boolean;
  c?: string;
  bc?: string;
  fs?: string | number;
  ff?: string;
}

