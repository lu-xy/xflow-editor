import {InlineNodeType} from "packages/types/node";
import {TextDelta} from "packages/types/text";
import {IInlineAttrs} from "packages/types/inline-attrs";
import {InlineLinkAttrs} from "packages/inlines";

export interface IInlineModel {
  type: string
  nodeType: InlineNodeType
  attrs: IInlineAttrs
}

// TODO mention data
export interface IMentionModel extends IInlineModel {
  type: 'mention'
  nodeType: 'inlineVoid'
  attrs: {
    textDeltas: Array<TextDelta>
    type: 'user' | 'doc' | 'chatCard'
    mentionId: string | number
  }
}

export interface ILinkModel extends IInlineModel {
  type: 'link'
  nodeType: 'inlineVoid',
  attrs: InlineLinkAttrs
}
