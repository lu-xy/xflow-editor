import {TextDelta} from "packages/types/text";
import {IBlockProps} from "packages/types/block-props";
import {ITableBlockProps} from "packages/blocks";
import {BlockNodeType} from "packages/types/node";

export interface IBlockModel{
  type: string
  id: string
  nodeType: BlockNodeType
  children?: Array<IBlockModel | TextDelta>,
  meta?: {
    align: 'left' | 'center' | 'right'
    folded: boolean
    indent: number
    [key: string]: string | number | boolean | object
  },
  props: IBlockProps
}

export interface ITextBlockModel extends IBlockModel{
  nodeType: 'editable'
  children: TextDelta[]
}

export interface IHeadingOneBlockModel extends ITextBlockModel {
  type: 'heading-one'
}

export interface IHeadingTwoBlockModel extends ITextBlockModel  {
  type: 'heading-two'
}

export interface IBlockQuoteModel extends ITextBlockModel  {
  type: 'block-quote'
}

export interface IBulletBlockModel extends ITextBlockModel {
  type: 'bullet'
}

export interface IOrderedBlockModel extends ITextBlockModel  {
  type: 'ordered'
}

export interface IImageBlockModel extends IBlockModel {
  type: 'image'
  nodeType: 'block',
  children: IBlockModel[]
  props: {
    src: string
    width: number
    align: 'left' | 'center' | 'right'
  }
}

export interface ICodeBlockModel extends ITextBlockModel {
  type: 'code'
  props: {
    language: string,
    textDeltas: Array<TextDelta>
  }
}

export interface ITodoBlockModel extends ITextBlockModel {
  type: 'todo'
  props: {
    done: boolean
    doneAt: number
    createdAt: number
    stopAt: number
    syncTaskId: string
    members?: string[]
    textDeltas: Array<TextDelta>
  }
}

// export interface IQuoteBlockModel extends IBlockModel {
//   type: 'quote'
//   nodeType: 'block'
//   children: ITextModel[]
//   props: {
//     documentId: string
//   }
// }

export interface ICalloutBlockModel extends ITextBlockModel {
  type: 'callout'
  props: {
    type: 'info' | 'warning' | 'danger' | 'success'
    flag: string,
    textDeltas: Array<TextDelta>
  }
}

export interface IDividerBlockData extends IBlockModel {
  type: 'divider'
  nodeType: 'block'
}

export interface ITableCellBlockModel extends ITextBlockModel {
  type: 'table-cell'
}

export interface ITableRowBlockModel extends IBlockModel {
  type: 'table-row'
  nodeType: 'block'
  props: {},
  children: ITableCellBlockModel[]
}

export interface ITableBlockModel extends IBlockModel {
  type: 'table'
  props: ITableBlockProps
  nodeType: 'block'
  children: ITableRowBlockModel[]
}




