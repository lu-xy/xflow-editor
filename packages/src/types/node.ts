export type RootNodeType = 'root'

export type BlockNodeType = 'block' | 'void' | 'editable'

export type InlineNodeType = 'inline' | 'inlineVoid' | 'text'

export type NodeType = RootNodeType | BlockNodeType | InlineNodeType
