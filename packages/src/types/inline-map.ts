import {ILinkModel} from "./inline-model";

export interface InlineMap {
  'link': ILinkModel
}
