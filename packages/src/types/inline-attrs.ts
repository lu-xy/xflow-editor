import {TextDelta} from "packages/types/text";

export interface IInlineAttrs {
  textDeltas: Array<TextDelta>
  [key: string]: any
}
