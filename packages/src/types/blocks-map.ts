import {
  IBlockModel,
  IHeadingOneBlockModel,
  IHeadingTwoBlockModel,
  ITableBlockModel, ITableCellBlockModel,
  ITableRowBlockModel
} from "./block-model";

export interface IBlockMap {
  'h1': IHeadingOneBlockModel
  'h2': IHeadingTwoBlockModel
  'table': ITableBlockModel
  'table-row': ITableRowBlockModel
  'table-cell': ITableCellBlockModel

  [key: string]: IBlockModel
}
