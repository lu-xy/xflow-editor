import {IInlineAttrs} from "packages/types";

export interface InlineLinkAttrs extends IInlineAttrs {
  textDeltas: []
  href: string
  type: 'doc' | 'uri'
}
