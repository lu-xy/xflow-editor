import {IInlineSchema} from "packages/core";
import {InlineLinkComponent, InlineLinkAttrs} from "packages/inlines";

export const InlineLink: IInlineSchema<InlineLinkAttrs> =  {
  type: "link",
  nodeType: 'inline',
  attrs: {
    href: '',
    textDeltas: [],
    type: 'uri'
  },
  render: InlineLinkComponent
}
