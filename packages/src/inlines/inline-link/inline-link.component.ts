import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BaseInline} from "packages/core";
import {InlineLinkAttrs} from "packages/inlines";

@Component({
  selector: 'xf-inline-link',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './inline-link.component.html',
  styleUrls: ['./inline-link.component.scss']
})
export class InlineLinkComponent extends BaseInline<InlineLinkAttrs> {

}
