import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TextBlockComponent} from "packages/blocks/text-block";

@Component({
  selector: 'td[xf-table-cell-block]',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './table-cell-block.component.html',
  styleUrls: ['./table-cell-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableCellBlockComponent extends TextBlockComponent {

}
