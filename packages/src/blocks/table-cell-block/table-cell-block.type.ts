import {ITextBlockModel} from "packages/types";

export interface ITableCellBlockModel extends ITextBlockModel {
  type: 'table-cell'
}
