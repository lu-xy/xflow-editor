import {IBlockSchema} from "packages/core";
import {TableCellBlockComponent} from "packages/blocks";

export const TableCellBlock: IBlockSchema<{}> = {
  type: 'table-cell',
  nodeType: 'editable',
  render: TableCellBlockComponent,
  children: [],
  props: {}
}
