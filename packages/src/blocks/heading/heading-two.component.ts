import {ChangeDetectionStrategy, Component} from "@angular/core";
import {TextBlockComponent} from "packages/blocks/text-block/text-block.component";
import {ITextBlockModel} from "packages/types";

export interface IHeadingTwoBlockModel extends ITextBlockModel  {
  type: 'heading-two'
}
@Component({
  selector: 'h2',
  template: `<ng-container #childrenContainer></ng-container>`,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadingTwoBlockComponent extends TextBlockComponent {

}
