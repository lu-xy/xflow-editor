import {IBlockSchema} from "packages/core";
import {HeadingOneBlockComponent, HeadingThreeBlockComponent, HeadingTwoBlockComponent} from "packages/blocks";

export const HeadingOneBlock: IBlockSchema<{}> = {
  type: 'heading-one',
  nodeType: 'editable',
  render: HeadingOneBlockComponent,
  children: [],
  props: {}
}

export const HeadingTwoBlock: IBlockSchema<{}> = {
  type: 'heading-two',
  nodeType: 'editable',
  render: HeadingTwoBlockComponent,
  children: [],
  props: {}
}

export const HeadingThreeBlock: IBlockSchema<{}> = {
  type: 'heading-three',
  nodeType: 'editable',
  render: HeadingThreeBlockComponent,
  children: [],
  props: {}
}
