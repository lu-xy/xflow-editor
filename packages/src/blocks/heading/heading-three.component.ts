import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TextBlockComponent} from "packages/blocks";
import {ITextBlockModel} from "packages/types";

export interface IHeadingThreeBlockModel extends ITextBlockModel  {
  type: 'heading-three'
}
@Component({
  selector: 'h3',
  standalone: true,
  imports: [CommonModule],
  template: '<ng-container #childrenContainer></ng-container>',
  styles: ['']
})
export class HeadingThreeBlockComponent extends TextBlockComponent{

}
