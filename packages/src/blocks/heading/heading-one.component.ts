import {ChangeDetectionStrategy, Component} from "@angular/core";
import {TextBlockComponent} from "packages/blocks";
import {ITextBlockModel} from "packages/types";

export interface IHeadingOneBlockModel extends ITextBlockModel {
  type: 'heading-one'
}
@Component({
  selector: 'h1',
  template: `<ng-container #childrenContainer></ng-container>`,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadingOneBlockComponent extends TextBlockComponent {

}
