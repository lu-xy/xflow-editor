import {
  ChangeDetectionStrategy,
  Component, ComponentRef, ElementRef, HostListener, ViewChild, ViewContainerRef,
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseBlock} from "packages/core/components";
import {ITextBlockModel, ITextStyles} from "packages/types";
import {createView, ICharacterRange} from "packages/core/utils";
import {formatText} from "packages/core/utils";
import * as Y from "yjs";

@Component({
  selector: 'div[xf-text-block]',
  standalone: true,
  imports: [CommonModule],
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextBlockComponent<Model extends ITextBlockModel = ITextBlockModel> extends BaseBlock<Model> {
  @ViewChild('textContainer', {read: ViewContainerRef, static: true}) textContainer!: ViewContainerRef
  @ViewChild('textContainer', {read: ElementRef, static: true}) textContainerEle!: ElementRef

  inlineComponentCollection = new Map<string, ComponentRef<BaseBlock>>()

  get yText() {
    return this.yChildren as Y.Text
  }

  ngAfterViewInit() {
    this.render()
  }

  render() {
    if (!this.children?.length) this.children.splice(0, 0, {insert: '&#xFEFF;'})
    const container = (this.textContainerEle?.nativeElement || this.hostEl.nativeElement) as HTMLDivElement
    this.inlineComponentCollection.forEach(c => c.destroy())
    this.inlineComponentCollection.clear()
    container.innerHTML = ''
    this.children.forEach(delta => {
      const textNode = createView(delta, this.id, this.vcr, this.editor.registry)
      if (!textNode) return
      if(textNode instanceof HTMLElement) {
        container.appendChild(textNode)
      } else {
        this.inlineComponentCollection.set(textNode.instance.id, textNode)
        container.appendChild(textNode.location.nativeElement)
      }
    })
  }

  override onResetChildren() {
    if(!this.editor.undoRedo$.value.isUndoOrRedoing) return
    super.onResetChildren()
    this.render()
  }

  format = <T extends keyof ITextStyles>(range: ICharacterRange, format: T, value: ITextStyles[T]) => {
    // formatTextModel(this.children!, range, format, value)
    // this.render()
    // setSelectionRange(range, this.hostEl.nativeElement)
    const container = this.textContainerEle?.nativeElement || this.hostEl.nativeElement
    formatText(container, range, format, value + '')
    this.yText.format(range.start, range.end - range.start, {[format]: value})
    console.log('format', this.yText.toDelta())
  }

}

