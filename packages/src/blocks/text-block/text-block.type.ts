import {IBlockModel, TextDelta} from "packages/types";

export interface ITextBlockModel extends IBlockModel{
  nodeType: 'editable'
  children: TextDelta[]
}
