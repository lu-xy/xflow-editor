import {IBlockSchema} from "packages/core";
import {ITableBlockProps, TableBlockComponent} from "packages/blocks";

export const TableBlock: IBlockSchema<ITableBlockProps> = {
  type: 'table',
  nodeType: 'block',
  render: TableBlockComponent,
  children: ['table-row'],
  props: {
    colWidths: [100]
  },
  beforeCreate: (rowAndCol: [number, number]) => {
    return {
      props: {
        colWidths: Array.from({length: rowAndCol[1]}, () => 100)
      },
      children: Array.from({length: rowAndCol[0]}, () => {
        return {
          type: 'table-row',
          params: rowAndCol[1]
        }
      })
    }
  }
}
