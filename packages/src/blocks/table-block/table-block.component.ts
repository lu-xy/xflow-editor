import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseBlock} from "packages/core/components";
import {ITableBlockModel} from "./table-block.type";
import {TableRowBlockComponent} from "packages/blocks";
import {RenderBlockDirective} from "packages/core";
import {IBlockModel} from "packages/types";

@Component({
  selector: 'div[xf-table-block]',
  standalone: true,
  imports: [CommonModule, TableRowBlockComponent, RenderBlockDirective],
  templateUrl: './table-block.component.html',
  styleUrls: ['./table-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableBlockComponent extends BaseBlock<ITableBlockModel> {

  trackBy = (index: number, item: IBlockModel) => {
    return item.type + '-' + item.id
  }

  addCol() {
    const childRef = this.editor.blockRefStore.get(this.children[0].id) as TableRowBlockComponent
    this.editor.yStore.doc.transact(() => {
      this.changeProps('colWidths', 100, 'push')
      this.changeProps('test', 100)
      childRef.addCell()
    })
  }

}
