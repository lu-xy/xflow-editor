import {IBlockModel, IBlockProps, ITableRowBlockModel} from "packages/types";

export interface ITableBlockModel extends IBlockModel {
  type: 'table'
  props: ITableBlockProps
  nodeType: 'block'
  children: ITableRowBlockModel[]
}

export interface ITableBlockProps extends IBlockProps {
  colWidths: number[];
}
