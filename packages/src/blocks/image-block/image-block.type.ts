import {IBlockModel, IBlockProps} from "packages/types";
import {ITextBlockModel} from "packages/blocks";

export interface IImageBlockModel extends IBlockModel {
  type: 'image'
  nodeType: 'block',
  children: ITextBlockModel[]
  props: {
    src: string
    width: number
    align: 'left' | 'center' | 'right'
  }
}

export interface IImageBlockProps extends IBlockProps {
  src: string;
  align: 'left' | 'center' | 'right';
  width: number;
}

