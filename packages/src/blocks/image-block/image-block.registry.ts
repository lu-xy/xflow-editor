import {IBlockSchema} from "packages/core";
import {ImageBlockComponent} from "packages/blocks/image-block/image-block.component";

export const ImageBlock: IBlockSchema<{}> = {
  type: 'table',
  nodeType: 'block',
  children: [],
  props: {
    src: '',
    align: 'center',
    width: 200
  },
  render: ImageBlockComponent,
}
