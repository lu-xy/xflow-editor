import {ChangeDetectionStrategy, Component} from '@angular/core';
import { CommonModule } from '@angular/common';
import {BaseBlock} from "packages/core/components";
import {IImageBlockModel} from "./image-block.type";

@Component({
  selector: 'xf-image-block',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './image-block.component.html',
  styleUrls: ['./image-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageBlockComponent extends BaseBlock<IImageBlockModel> {

}
