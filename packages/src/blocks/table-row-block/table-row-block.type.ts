import {IBlockModel, IBlockProps, ITableCellBlockModel} from "packages/types";

export interface ITableRowBlockModel extends IBlockModel {
  type: 'table-row'
  nodeType: 'block'
  props: ITableRowBlockProps,
  children: ITableCellBlockModel[]
}

export interface ITableRowBlockProps extends IBlockProps {
}
