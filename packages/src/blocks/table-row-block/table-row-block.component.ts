import {ChangeDetectionStrategy, Component, QueryList, ViewChildren} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseBlock, RenderBlockDirective} from "packages/core";
import {TableCellBlockComponent} from "packages/blocks";
import {IBlockModel, ITableRowBlockModel} from "packages/types";

@Component({
  selector: 'tr[xf-table-row-block]',
  standalone: true,
  imports: [CommonModule, TableCellBlockComponent, RenderBlockDirective],
  templateUrl: './table-row-block.component.html',
  styleUrls: ['./table-row-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableRowBlockComponent extends BaseBlock<ITableRowBlockModel> {

  trackBy = (index: number, item: IBlockModel) => {
    return item.type + '-' + item.id
  }

  addCell() {
    this.addChildren('table-cell')
  }

}
