import {ITableRowBlockProps} from "./table-row-block.type";
import {IBlockSchema} from "packages/core";
import {TableRowBlockComponent} from "./table-row-block.component";

export const TableRowBlock: IBlockSchema<ITableRowBlockProps> = {
  type: 'table-row',
  nodeType: 'block',
  render: TableRowBlockComponent,
  children: ['table-cell'],
  props: {},
  beforeCreate: (colCount: number) => {
    return {
      props: {},
      children: Array.from({length: colCount}, () => {
        return {type: 'table-cell'}
      })
    }
  }
};
