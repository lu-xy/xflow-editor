import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  inject,
  Input, NgZone,
  ViewContainerRef
} from "@angular/core";
import {IBlockModel} from "packages/types";
import {Editor} from "packages/core";
import {BehaviorSubject} from "rxjs";

@Directive({
  selector: '[xf-render-block]',
  standalone: true
})
export class RenderBlockDirective {
  @Input({required: true, alias: 'xf-render-block'}) data!: { model: IBlockModel, parentId: string, index: number, cdr: ChangeDetectorRef }

  isReady = new BehaviorSubject(false)
  editor = inject(Editor)

  // private zone = inject(NgZone);

  constructor(
    private vcr: ViewContainerRef,
    private hostEl: ElementRef,
  ) {
  }

  ngAfterViewInit() {
    this.render()
    this.data.cdr.detectChanges()
    // this.data.cdr.markForCheck()
    this.isReady.next(true)
    // this.zone.run(() => {
    //   this.data.cdr.detectChanges();
    //   this.isReady.next(true);
    // });
  }

  render() {
    if (!this.data) return
    const v = this.editor.createView(this.data.model, this.data.parentId, this.vcr)!
    if(this.hostEl.nativeElement instanceof HTMLElement) this.hostEl.nativeElement!.appendChild(v instanceof HTMLElement ? v : v.location.nativeElement)
  }
}
