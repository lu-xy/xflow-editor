import {BaseBlock} from "packages/core/components";

export class BlockRefStore {
  private store: Map<string, BaseBlock> = new Map();

  set = (id: string, ref: BaseBlock) => {
    this.store.set(id, ref);
  }

  get = (id: string) => {
    return this.store.get(id);
  }

}
