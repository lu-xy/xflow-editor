import {IBlockModel} from "packages/types";

export class ModelStore {

  blocksModelMap: Map<string, IBlockModel> = new Map()

  storeBlockModel(model: IBlockModel) {
    this.blocksModelMap.set(model.id, model)
  }

  getBlockModel(id: string) {
    return this.blocksModelMap.get(id)
  }

}
