import * as Y from 'yjs';
import {IBlockModel} from "packages/types";

export const blockModel2Y = (block: IBlockModel, cb?: (block: IBlockModel, yMap: Y.Map<any>) => void) => {
  let map: Y.Map<any>

  const propsMap = new Y.Map()
  for (const key in block.props) {
    const v = block.props[key]
    if (typeof v === 'object') {
      if (v instanceof Array) {
        const arr = new Y.Array()
        arr.push(v)
        propsMap.set(key, arr)
      } else {
        propsMap.set(key, new Y.Map(v))
      }
      continue
    }
    propsMap.set(key, block.props[key])
  }

  const metaMap = new Y.Map()
  if (block.meta) {
    for (const key in block.meta) {
      metaMap.set(key, block.meta[key])
    }
  }

  let childen
  if (block.children) {

    if (block.nodeType === 'editable') {
      childen = new Y.Text()
      // 覆盖默认的toJSON方法
      childen.toJSON = childen.toDelta
      childen.applyDelta(block.children)
    } else {
      childen = new Y.Array()
      childen.push(
        (block.children as IBlockModel[]).map(child => blockModel2Y(child, cb))
      )
    }
  }

  map = new Y.Map(
    [
      ['type', block.type],
      ['id', block.id],
      ['nodeType', block.nodeType],
      ['props', propsMap],
      ['children', childen],
      ['meta', metaMap]
    ]
  )

  cb && cb(block, map)
  return map
}
