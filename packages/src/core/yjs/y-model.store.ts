import * as Y from 'yjs';
import {IBlockModel} from "packages/types";
import {blockModel2Y} from "packages/core/yjs/createProxy";

type StoreItem = Y.Map<any>

export class YModelStore {
  public flatMapStore = new Map<string, StoreItem>()
  public yDocArray!: Y.Array<StoreItem>

  constructor(public readonly doc: Y.Doc) {
    this.yDocArray = doc.getArray('root')
  }

  blockModel2Y = <T extends IBlockModel>(block: T) => {
    return blockModel2Y(block, (block, yMap) => {
      this.set(block.id, yMap)
    })
  }

  get(key: string) {
    return this.flatMapStore.get(key)
  }

  set(key: string, value: StoreItem) {
    return this.flatMapStore.set(key, value)
  }


}
