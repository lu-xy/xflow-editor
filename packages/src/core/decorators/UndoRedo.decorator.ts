export function UndoRedo() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args: any[]) {
      console.log('undo redo', target)
      if (!target.editor.isUndoing) return
      const result = originalMethod.apply(this, args);
      return result;
    };
    return descriptor;
  };
}
