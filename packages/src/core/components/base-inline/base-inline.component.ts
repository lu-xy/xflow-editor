import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {IBlockProps} from "packages/types";

@Component({
  selector: 'xf-base-inline',
  standalone: true,
  imports: [CommonModule],
  template: '',
  styles: ['']
})
export class BaseInline<Attrs extends IBlockProps> {
  @Input({required: true}) attrs!: Attrs;



}
