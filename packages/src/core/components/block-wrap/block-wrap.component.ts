import {Component, HostBinding, inject} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Editor} from "packages/core/editor/editor/editor";

@Component({
  selector: '[xf-block-wrap]',
  standalone: true,
  imports: [CommonModule],
  template: ``,
  styles: [
    `:host {
      margin-bottom: 10px;
    }`
  ]
})
export class XFlowBlockWrap {
  @HostBinding('attr.contenteditable') contenteditable = 'false'

  editor = inject(Editor)

  constructor() {
  }

}
