import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  inject,
  Input,
  Output
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IBlockModel} from "packages/types";
import {RenderBlockDirective, XFlowBlockWrap} from "packages/core";

@Component({
  selector: '[xf-free-block]',
  standalone: true,
  imports: [CommonModule, XFlowBlockWrap, RenderBlockDirective],
  template: `
      <ng-container *ngFor="let item of model; index as idx; trackBy: trackBy">
          <div xf-block-wrap [xf-render-block]="{ model: item, parentId, index: idx, cdr }"></div>
      </ng-container>
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreeBlockComponent {
  @HostBinding('attr.contenteditable') contenteditable = 'true'
  @Input({required: true}) parentId!: string
  @Input() model: IBlockModel[] = []
  @Output() ready = new EventEmitter<boolean>()

  cdr = inject(ChangeDetectorRef)

  trackBy = (index: number, item: IBlockModel) => {
    return item.type + '-' + item.id
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      // this.renderBlocks.forEach(rb => {
      //   console.log('render block', rb.data.model.id, rb.isReady.value)
      // })
      this.ready.emit(true)
    })
  }

}
