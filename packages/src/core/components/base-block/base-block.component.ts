import {
  ChangeDetectorRef,
  Component, ElementRef,
  EventEmitter,
  HostBinding,
  inject,
  Input,
  Output, SimpleChange,
  ViewContainerRef
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IBlockMap, IBlockModel} from "packages/types";
import {Editor} from "packages/core/editor";
import * as Y from 'yjs'
import {UndoRedo} from "packages/core";

@Component({
  selector: 'div[xf-base-block]',
  standalone: true,
  imports: [CommonModule],
  template: ``,
})
export class BaseBlock<Model extends IBlockModel = IBlockModel> {
  @HostBinding('attr.contenteditable')
  get contenteditable() {
    return this.model.nodeType === 'editable' ? 'true' : 'false'
  }

  @Input({required: true}) model!: Model
  @Input({required: true}) parentId!: string

  yModel!: Y.Map<any>

  @HostBinding('id')
  get id() {
    return this.model!.id
  }

  @HostBinding('attr.xf-node-type')
  get nodeType() {
    return this.model!.nodeType as Model['nodeType']
  }

  get type() {
    return this.model!.type as Model['type']
  }

  get props() {
    return this.model!.props as Model['props']
  }

  get children() {
    return this.model.children as Model['children']
  }

  get yProps() {
    return this.yModel.get('props') as Y.Map<any>
  }

  get yChildren() {
    return this.yModel.get('children')
  }

  @Output() change = new EventEmitter()

  protected editor = inject(Editor)
  protected hostEl = inject(ElementRef)
  protected vcr = inject(ViewContainerRef)
  protected cdr = inject(ChangeDetectorRef)

  ngOnInit() {
    this.editor.blockRefStore.set(this.id, this)
    this.yModel = this.editor.yStore.get(this.id)!

    // this.yProps.observeDeep(this.onResetPropsBinder)
    // this.yChildren.observe(this.onResetChildrenBinder)
  }


  onResetPropsBinder = this.onResetProps.bind(this)
  onResetProps()  {
    // if (!this.editor.undoRedo$.value.isUndoOrRedoing) return
    console.log('props changed', this.yProps.toJSON())
    this.model.props = this.yProps.toJSON()
    this.cdr.detectChanges()
  }

  onResetChildrenBinder = this.onResetChildren.bind(this)
  onResetChildren() {
    // if (!this.editor.undoRedo$.value.isUndoOrRedoing) return
    console.log('children changed', this.yChildren.toJSON())
    this.model.children = this.yChildren.toJSON()
    this.cdr.detectChanges()
  }

  /**
   * 统一修改 props
   * @param key 要修改的prop的key
   * @param value 要修改的prop的值，如果这个prop是个对象，那么这个值是这个prop对象要修改的值
   * @param fieldOrFn 如果这个prop是个对象，那么这个值是这个prop对象要修改的字段，如果这个prop是个数组，那么这个值是要执行的方法
   * @param params 如果这个prop是个数组，那么这个值是要执行的方法的参数，`fieldOrFn` 为 'delete' 时，params 为 [deleteIndex, deleteCount]， fieldOrFn 为 'insert' 时，params 为 [index]， `value` 为要插入的值
   */
  changeProps(key: keyof Model['props'], value: any, fieldOrFn?: string | number | fnName, params?: number[]) {
    const prop = this.props[key]
    if (typeof prop === 'object') {

      if (Array.isArray(prop)) {
        const yProp = this.yProps.get(key as string) as Y.Array<any>

        try {
          const fn = fieldOrFn as fnName
          switch (fn) {
            case 'unshift':
              prop.unshift(value)
              yProp.unshift([value])
              return;
            case 'push':
              prop.push(value)
              yProp.push([value])
              return;
            case 'delete':  // params: [deleteIndex, deleteCount]
              const [from, count] = params as [number, number]
              prop.splice(from, count)
              yProp.delete(from, count)
              return;
            case 'insert':
              const [index] = params as [number]
              prop.splice(index, 0, ...value)
              yProp.insert(index, value)
              return;
          }
        } catch (e) {
          console.error(e)
        }

      } else {
        const yProp = this.yProps.get(key as string) as Y.Map<any>
        const filed = fieldOrFn as string
        // @ts-ignore
        prop[filed] = value
        yProp.set(filed, value)
      }

      return
    }

    this.yProps.set(key as string, value)
  }

  addChildren(children: keyof IBlockMap, params?: Parameters<typeof this.editor.createBlockModel>[1], index?: number) {
    const cb = this.editor.createBlockModel(children, params)
    const yCb = this.editor.yStore.blockModel2Y(cb)
    index ? this.children!.splice(index, 0, cb) : this.children!.push(cb)
    index ? this.yChildren.insert(index, [yCb]) : this.yChildren.push([yCb])
    this.cdr.detectChanges()
  }

  detectChanges() {
    this.cdr.detectChanges()
  }

  ngOnDestroy() {
    // this.yProps?.unobserveDeep(this.onResetPropsBinder)
    // this.yChildren?.unobserve(this.onResetChildrenBinder)
  }
}

type fnName = 'unshift' | 'push' | 'delete' | 'insert'
