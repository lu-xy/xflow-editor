import {Type} from "@angular/core";
import {BaseBlock, BaseInline} from "packages/core/components";
import {BlockNodeType, IBlockProps, IInlineAttrs, InlineNodeType, NodeType} from "packages/types";

export interface IBlockSchema<T extends IBlockProps> {
  type: string
  nodeType: BlockNodeType
  render: Type<BaseBlock>
  children?: string[],
  props: T,
  beforeCreate?: (...params: any[]) => { props: T, children: Array<{ type: string, params?: any }> }
}

/**
 * A is the attrs type of the inline node
 */
export interface IInlineSchema<A extends IInlineAttrs> {
  type: string
  nodeType: InlineNodeType
  render: Type<BaseInline<A>>
  attrs: A
}
