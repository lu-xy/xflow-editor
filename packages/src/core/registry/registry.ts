import {IBlockSchema, IInlineSchema} from "packages/core";
import {IBlockProps, IInlineAttrs} from "packages/types";

export class Registry {

  private registeredBlock = new Map<string, IBlockSchema<IBlockProps>>();
  private registeredInline = new Map<string, IInlineSchema<IInlineAttrs>>();

  constructor(blockRegistry: IBlockSchema<IBlockProps>[], inlineRegistry: IInlineSchema<IInlineAttrs>[]) {
    blockRegistry.forEach(block => this.registerBlock(block.type, block));
    inlineRegistry.forEach(inline => this.registerInline(inline.type, inline));
  }

  registerBlock(key: string, block: IBlockSchema<IBlockProps>) {
    this.registeredBlock.set(key, block);
  }

  registerInline(key: string, inline: IInlineSchema<IInlineAttrs>) {
    this.registeredInline.set(key, inline);
  }

  getBlock(key: string) {
    return this.registeredBlock.get(key)
  }

  getInline(key: string) {
    return this.registeredInline.get(key)
  }
}
