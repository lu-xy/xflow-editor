import {BlockRefStore, Registry, YModelStore} from "packages/core";
import {ChangeDetectorRef, ViewContainerRef} from "@angular/core";
import {IBlockMap, IBlockModel} from "packages/types";
import {createBlock, createView} from "packages/core/utils";
import * as Y from 'yjs'
import {BehaviorSubject} from "rxjs";

interface CreateBlockParamsList {
  'heading-one': undefined
  'heading-two': undefined
  'heading-three': undefined
  'table': [number, number]
  'table-row': number

  [key: string]: any
}

export class Editor {

  constructor(
    public readonly registry: Registry,
    public readonly blockRefStore: BlockRefStore,
  ) {
  }

  undoRedo$ = new BehaviorSubject<{ isUndoOrRedoing: boolean, type: 'undo' | 'redo' }>({isUndoOrRedoing: false, type: 'undo'})
  yStore!: YModelStore
  undoManager!: Y.UndoManager

  toJSON = () => {
    return this.yStore.yDocArray.toJSON()
  }

  insertBlocks = (blocks: IBlockModel[], index?: number) => {
    const bs = blocks.map(block => this.yStore.blockModel2Y(block))
    index ? this.yStore.yDocArray.insert(index, bs) : this.yStore.yDocArray.push(bs)
  }

  createView = (node: IBlockModel, parentId: string, vcr: ViewContainerRef) => {
    return createView(node, parentId, vcr, this.registry)
  }

  createBlockModel = <T extends keyof IBlockMap>(type: T, params?: CreateBlockParamsList[T]): IBlockMap[T] => {
    return createBlock(type, this.registry, params)
  }


}
