import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  HostBinding,
  HostListener,
  inject, Input, SimpleChanges,
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Registry} from "packages/core/registry/registry";
import {IBlockModel} from "packages/types";
import {FreeBlockComponent} from "packages/core/components/free-block/free-block.component";
import {
  HeadingOneBlock,
  HeadingTwoBlock,
  TableBlock, TableBlockComponent, TableRowBlock,
  TableCellBlock, TextBlockComponent
} from "packages/blocks";
import {BlockRefStore} from "packages/core/model/blockRefStore";
import {FormsModule} from "@angular/forms";
import {getSelectionRange} from "packages/core/utils/selection";
import {createBlock} from "packages/core/utils";
import {Editor} from "./editor";
import {YModelStore} from "packages/core";
import * as Y from 'yjs'
import {AbstractType} from "yjs";

interface ChangeRecord {
  id: string
  field: 'props' | 'children'
}

const closetMapId = (map: AbstractType<any>): string => {
  if (map instanceof Y.Map && map.has('id'))
    return map.get('id') as string
  return map.parent ? closetMapId(map.parent) : 'root'
}
const closetChangeField = (e: Y.YEvent<any>): 'props' | 'children' => {
  return e.path ? (e.path.reverse().find(p => p === 'props' || p === 'children') as 'props' | 'children') : "children"
}

const recordChange = (events: Y.YEvent<any>[]): ChangeRecord[] => {
  return events.map(e =>
    ({id: closetMapId(e.target), field: closetChangeField(e)})
  )
}

// 去重相同的记录
const dedupRecord = (records: ChangeRecord[]): ChangeRecord[] => {
  return records.reduce((acc, cur) => {
    if (!acc.find(r => r.id === cur.id && r.field === cur.field)) acc.push(cur)
    return acc
  }, [] as ChangeRecord[])
}

@Component({
  selector: 'div[xf-editor][xf-node-type="root"]',
  standalone: true,
  imports: [CommonModule, FreeBlockComponent, FormsModule],
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: Editor,
      useFactory: () => {
        return new Editor(
          new Registry(
            [HeadingOneBlock, HeadingTwoBlock, TableBlock, TableRowBlock, TableCellBlock],
            []
          ),
          new BlockRefStore()
        )
      },
      deps: []
    }
  ]
})
export class XFlowEditor {
  @HostBinding('id') id = 'editor-root-01'

  _model!: IBlockModel[]
  @Input('model')
  set model(val: IBlockModel[]) {
    this._model = val
  }

  get model() {
    return this._model
  }

  input1 = '2'
  input2 = '3'

  editor = inject(Editor)

  constructor(
    protected cdr: ChangeDetectorRef
  ) {
    this.editor.yStore = new YModelStore(new Y.Doc({guid: this.id}))
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['model'] && changes['model'].firstChange) {
      this.editor.insertBlocks(this.model, 0)
    }
  }

  lastUpdateRecordArr: ChangeRecord[] = []

  onReady() {
    this.editor.undoManager = new Y.UndoManager(this.editor.yStore.yDocArray)

    this.editor.yStore.yDocArray.observeDeep(e => {
      if(this.editor.undoRedo$.value.isUndoOrRedoing) return
      this.lastUpdateRecordArr = dedupRecord(recordChange(e))
      console.log('doc array changed', e, this.lastUpdateRecordArr)
    })

    this.editor.undoManager.on('stack-item-added', async (event) => {
      if(event.type === 'redo') return
      event.stackItem.meta.set('recordArr', this.lastUpdateRecordArr)
      console.log('undo stack item added', event, event.stackItem.meta)
      this.lastUpdateRecordArr = []
    })

    this.editor.undoManager.on('stack-item-popped', async (event) => {
      console.log('undo stack item popped', this.editor.yStore.yDocArray.toJSON())
      const recordArr = event.stackItem.meta.get('recordArr') as ChangeRecord[]
      if (!recordArr?.length) return
      recordArr.forEach(r => {
        if(r.id === 'root') {
          this.model = this.editor.yStore.yDocArray.toJSON()
          this.cdr.detectChanges()
          return
        }
        const br = this.editor.blockRefStore.get(r.id)
        if (!br) return
        if (r.field === 'props') br.onResetProps()
        if (r.field === 'children') br.onResetChildren()
      })
    })
  }


  @HostListener('keydown.meta.z', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    e.preventDefault()
    this.editor.undoRedo$.next({isUndoOrRedoing: true, type: 'undo'})
    const undo = this.editor.undoManager.undo()
    requestAnimationFrame(() => {
      this.editor.undoRedo$.next({isUndoOrRedoing: false, type: 'undo'})
    })
  }

  @HostListener('beforeinput', ['$event'])
  onBeforeInput(e: InputEvent) {
    // console.log('before input', e)
  }

  @HostListener('input', ['$event'])
  onInput(e: InputEvent) {
    const target = e.target as HTMLElement
    const targetBlockId = target.closest('[xf-node-type="block"]')!.id
    // const rootBlockId = target.closest('[xf-block-root]')?.id.split('-block-root')[0]
    // if (!rootBlockId) return
    // const targetBlockId = target.closest('[xf-node-type="block"]')?.id
    // if (!targetBlockId) return
    // const block = this.model.find(b => b.id === rootBlockId)
    // if (rootBlockId === targetBlockId) {
    //   console.log('input block', this.editor.blockRefStore.get(rootBlockId)!.takeSnapshot())
    //   return
    // }
    // if (!block) return
    // const targetBlock = this.findBlockFromChildren(targetBlockId, block)
  }

  findBlockFromChildren(id: string, parent: IBlockModel):
    IBlockModel | null {
    if (parent.id === id) return parent
    if (parent.children) {
      for (const child of parent.children) {
        const cb = child as IBlockModel
        if (cb.id === id) return cb
        if (cb.children) {
          return this.findBlockFromChildren(id, cb)
        }
      }
    }
    return null
  }

  onClick() {
    this.model.splice(1, 1, createBlock('heading-one', this.editor.registry))
  }

  onClick2() {
    const id = this.model.find(b => b.type === 'table')!.id
    const bRef = this.editor.blockRefStore.get(id)! as TableBlockComponent
    bRef.addCol()
  }

  onClick3() {
    const bs = this.editor.createBlockModel('table', [parseInt(this.input1), parseInt(this.input2)])
    this.editor.insertBlocks([bs])
    this.model.push(bs)
    // this.model = this.editor.yStore.yDocArray.toJSON()
    // this.cdr.detectChanges()
    // this.freeBlock.cdr.detectChanges()
  }

  onClick4(e: MouseEvent) {
    e.preventDefault()
    const ae = document.activeElement as HTMLElement
    const cr = this.editor.blockRefStore.get(ae.closest('[xf-node-type="editable"]')!.id)! as TextBlockComponent<any>
    cr.format(getSelectionRange(), 'c', 'red')
  }

  onClick5(e: MouseEvent) {
    e.preventDefault()
    const ae = document.activeElement as HTMLElement
    console.log(ae)
    const cr = this.editor.blockRefStore.get(ae.closest('[xf-node-type="editable"]')!.id)! as TextBlockComponent<any>
    const {start, end} = getSelectionRange()
    cr.format(getSelectionRange(), 'bold', true)
  }

  onClick6(e: MouseEvent) {
    e.preventDefault()
    const ae = document.activeElement as HTMLElement
    const cr = this.editor.blockRefStore.get(ae.closest('[xf-node-type="editable"]')!.id)! as TextBlockComponent<any>
    const {start, end} = getSelectionRange()
    cr.format(getSelectionRange(), 'code', true)
  }

  onGetData(e: MouseEvent) {
    e.preventDefault()
    console.log(this.editor.yStore.yDocArray.toJSON())
  }

}
