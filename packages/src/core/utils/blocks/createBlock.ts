import {Registry} from "packages/core";
import {IBlockMap} from "packages/types";

export const genBlockId = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4();
}

export const createBlock = <T extends keyof IBlockMap>(type: T, registry: Registry, params?: any): IBlockMap[T] => {
  const register = registry.getBlock(type as string)!
  const createBefore = register.beforeCreate?.(params)
  const children = createBefore?.children?.map(c => createBlock(c.type, registry, c.params)) || register.children?.map(c => createBlock(c, registry))
  return {
    id: genBlockId(),
    type: register.type,
    nodeType: register.nodeType,
    children,
    props: createBefore?.props || register.props
  }
}
