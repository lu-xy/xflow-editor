export const createProxy = <T extends object>(data: T, deep = false): T => {
  if(!data) return data
  return new Proxy(data, {
    set(target: any, p: string | symbol, newValue: any, receiver: any): boolean {
      if(!(target instanceof Array && p === 'length')) console.log('set', target, p, target[p], newValue);
      return Reflect.set(target, p, newValue, receiver);
    },
    get(target: any, p: string | symbol, receiver: any): any {
      const value = Reflect.get(target, p, receiver)
      if (deep && typeof value === 'object') {
        return createProxy(value)
      }
      return value
    }
  })
}
