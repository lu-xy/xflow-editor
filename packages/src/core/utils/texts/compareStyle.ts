import {ITextStyles, TextDelta} from "packages/types";

export const compareTextDeltaStyle = (a: TextDelta, b: TextDelta) => {
  if (!a.attributes && !b.attributes) return true
  if( (a.attributes && !b.attributes) || (!a.attributes && b.attributes)) return false
  const aKeys = Object.keys(a.attributes!)
  const bKeys = Object.keys(b.attributes!)
  if (aKeys.length !== bKeys.length) return false
  return Object.keys(a.attributes!).every(key => a.attributes![key as keyof ITextStyles] === b.attributes![key as keyof ITextStyles])
}

