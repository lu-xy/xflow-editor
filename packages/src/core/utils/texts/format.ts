// import {IBlockModel, ITextModel, ITextStyles} from "packages/types";
// import {compareStyle, findPathByCharacterIndex, ICharacterRange, setSelectionRange} from "packages/core/utils";
//
// export const formatTextModel = <T extends keyof ITextStyles>(model: (ITextModel | IBlockModel)[], range: ICharacterRange, format: T, value: ITextStyles[T]) => {
//   const {start, end } = range
//   if (start === end) return
//
//   const fromPath = findPathByCharacterIndex(model, start)
//   const toPath = findPathByCharacterIndex(model, end)
//   console.log(start, end, fromPath, toPath)
//
//   for (let i = fromPath[0] + 1; i < toPath[0]; i++) {
//     const child = model[i]
//     if ('type' in child) continue;
//     child.styles ||= {}
//     child.styles[format] = value
//   }
//
//   if (fromPath[0] === toPath[0]) {
//     const fromChild = model[fromPath[0]] as ITextModel
//     const middleText = fromChild.text.slice(fromPath[1], toPath[1])
//     const rightText = fromChild.text.slice(toPath[1])
//     fromChild.text = fromChild.text.slice(0, fromPath[1])
//
//     model.splice(fromPath[0] + 1, 0, {
//       text: middleText,
//       styles: {
//         ...fromChild.styles,
//         [format]: value
//       }
//     })
//
//     if (rightText.length) {
//       model.splice(fromPath[0] + 2, 0, {
//         text: rightText,
//         styles: {
//           ...fromChild.styles,
//         }
//       })
//     }
//   } else {
//
//     let addFlag = 0
//
//     const fromChild = model[fromPath[0]] as ITextModel
//     console.log(fromChild)
//     const rightText = fromChild.text.slice(fromPath[1])
//     fromChild.text = fromChild.text.slice(0, fromPath[1])
//     if (rightText.length) {
//       model.splice(fromPath[0] + 1, 0, {
//         text: rightText,
//         styles: {
//           ...fromChild.styles,
//           [format]: value
//         }
//       })
//       addFlag++
//     }
//
//     const toChild = model[toPath[0] + addFlag] as ITextModel
//     const leftText = toChild.text.slice(0, toPath[1])
//     toChild.text = toChild.text.slice(toPath[1])
//     if (leftText.length) {
//       model.splice(toPath[0] + addFlag, 0, {
//         text: leftText,
//         styles: {
//           ...toChild.styles,
//           [format]: value
//         }
//       })
//     }
//   }
//
//   mergeAdjacentSameStyleText(model)
//   console.log('format', model)
// }
//
// export const mergeAdjacentSameStyleText = (model: (ITextModel | IBlockModel)[]) => {
//   let i = 0
//   while (i < model.length - 1) {
//     const cur = model[i] as ITextModel
//     const next = model[i + 1] as ITextModel
//     if (!cur.text) {
//       model.splice(i, 1)
//       continue
//     }
//     if (!next.text) {
//       model.splice(i + 1, 1)
//       continue
//     }
//     if (compareStyle(cur, next)) {
//       cur.text += next.text
//       model.splice(i + 1, 1)
//     } else {
//       i++
//     }
//   }
// }


import {ITextStyles} from "packages/types";
import {ICharacterRange, setSelectionRange} from "packages/core/utils";

export const formatText = <T extends keyof ITextStyles>(node: HTMLElement, range: ICharacterRange, format: string, value: `${ITextStyles[T]}`) => {
  setSelectionRange(range, node)
  const wRange = window.getSelection()!.getRangeAt(0)

  const parent = wRange.startContainer.parentNode!
  // 如果range包裹的节点有父节点，那么需要将父节点拆分，确保commonAncestorContainer的子孙节点只有一层
  if (wRange.startContainer === wRange.endContainer && parent instanceof HTMLElement && parent!.getAttribute('xf-node-type') !== 'block') {

    if (parent.getAttribute(`xf-style-${format}`) === value) return
    const splitText = parent.textContent!.slice(wRange.startOffset, wRange.endOffset)
    const leftText = parent.textContent!.slice(0, wRange.startOffset)
    const rightText = parent.textContent!.slice(wRange.endOffset)
    const cloneParent2 = parent.cloneNode() as HTMLElement
    const cloneParent3 = parent.cloneNode() as HTMLElement
    parent.textContent = leftText
    cloneParent2.textContent = rightText
    cloneParent3.setAttribute(`xf-style-${format}`, value)
    cloneParent3.textContent = splitText
    parent.after(cloneParent2)
    cloneParent2.before(cloneParent3)
    wRange.selectNodeContents(cloneParent3)
  } else {

    const selContent = wRange.cloneContents()
    const childNodes = selContent.childNodes

    for (let i = 0; i < childNodes.length; i++) {
      const node = childNodes[i]
      if (node instanceof Text) {
        const span = document.createElement('span')
        span.setAttribute('xf-node-type', 'text')
        span.setAttribute(`xf-style-${format}`, value)
        if (node.textContent === '') {
          selContent.removeChild(node)
          continue
        }
        span.textContent = node.textContent
        selContent.replaceChild(span, node)
      } else if (node instanceof HTMLElement) {
        // if(node.getAttribute(`xe-${tag}`)) node.removeAttribute(`xe-${tag}`)
        node.setAttribute(`xf-style-${format}`, value)
      }
    }
    wRange.deleteContents()
    wRange.insertNode(selContent)
  }

  mergeAdjacentSameTags(wRange.commonAncestorContainer, format, value)
}

export const mergeAdjacentSameTags = <T extends keyof ITextStyles>(container: Node, format: string, value: `${ITextStyles[T]}`) => {
  // 清理空文本节点
  for (let i = 0; i < container.childNodes.length; i++) {
    const node = container.childNodes[i]
    if (node.textContent === '') node.remove()
  }
  // 整理合并相邻的标签
  for (let i = 0; i < container.childNodes.length; i++) {
    const node = container.childNodes[i]
    if (node instanceof HTMLElement && (node.getAttribute(`xf-style-${format}`) === value)) {
      let j = i + 1
      while (j < container.childNodes.length) {
        const nextNode = container.childNodes[j] as HTMLElement
        // 两者都有xe-tag属性，且所有的xe-tag属性值都相同
        if (nextNode instanceof HTMLElement && isSameTag(node, nextNode)) {

          node.textContent! += nextNode.textContent
          nextNode.remove()
        } else {
          break
        }
      }
    }
  }
}

export const isSameTag = (node: HTMLElement, node2: HTMLElement) => {
  // 对比所有的xe-tag属性值是否相同
  const attrs = node.attributes
  const attrs2 = node2.attributes
  if (attrs.length !== attrs2.length) return false
  for (let i = 0; i < attrs.length; i++) {
    const attr = attrs[i]
    if (attr.name.startsWith('xf-style') && attr.value !== attrs2.getNamedItem(attr.name)?.value) return false
  }
  return true
}

// import {ITextStyles} from "packages/types";
// import {ICharacterRange, setSelectionRange} from "packages/core/utils";
//
// export const formatText = <K extends keyof ITextStyles>(node: HTMLElement, range: ICharacterRange, attr: string, value: ITextStyles[K]) => {
//   setSelectionRange(range, node)
//   textFormatter(window.getSelection()!.getRangeAt(0), attr, value)
// }
//
// export const textFormatter = <K extends keyof ITextStyles>(range: Range, attr: string, value: ITextStyles[K]) => {
//   const selection = range.cloneContents()
//   const el = document.createElement('span')
//   el.setAttribute('xf-style-' + attr, value + '')
//   el.appendChild(selection)
//   el.innerHTML = removeTagInHTML(attr, el.innerHTML)
//   range.deleteContents()
//   range.insertNode(el)
// }
//
// export const removeTagInHTML = (tag: string, html: string) => {
//   return html.replace(new RegExp(`<\/?${tag}[^>]*>`, 'g'), '')
// }
