import {IBlockModel, InsertEmbedDelta, InsertTextDelta, ITextStyles, TextDelta} from "packages/types";
import {ViewContainerRef} from "@angular/core";
import {Registry} from "packages/core/registry/registry";

export const createView = (node: IBlockModel | TextDelta, parentId: string, vcr: ViewContainerRef, registry: Registry) => {
  if (!('type' in node) && typeof node.insert === 'string') return createText(node as InsertTextDelta)
  return createComponent(node as IBlockModel | InsertEmbedDelta, parentId, vcr, registry)
};

export const createComponent = (node: IBlockModel | InsertEmbedDelta, parentId: string, vcr: ViewContainerRef, registry: Registry) => {
  if ('type' in node) {
    const render = registry.getBlock(node.type)?.render
    if (!render) return
    const cr = vcr.createComponent(render)
    if (!cr) return
    cr.instance.parentId = parentId
    cr.setInput('model', node)
    return cr
  } else {
    const type = Object.keys(node.insert)[0]
    const render = registry.getInline(type)?.render
    if (!render) return
    const cr = vcr.createComponent(render)
    if (!cr) return
    cr.setInput('attrs', node.attributes)
    return cr.location.nativeElement as HTMLElement
  }
}

export const createText = (node: InsertTextDelta) => {
  const span = document.createElement('span') as HTMLElement
  span.innerHTML = node.insert
  return bindAttrAndStyle(span, node.attributes || {})
}

export const bindAttrAndStyle = (ele: HTMLElement, attributes: ITextStyles) => {
  ele.setAttribute('xf-node-type', 'text')
  if (!attributes) return ele
  Object.entries(attributes).forEach(([key, value]) => {
    value && ele.setAttribute('xf-style-' + key, value + '')
  })
  return ele
}


